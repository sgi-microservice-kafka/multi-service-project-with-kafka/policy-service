package com.sgi.producer.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.listener.ConsumerSeekAware;
import org.springframework.stereotype.Component;

@Component
public class PolicyConsumer implements ConsumerSeekAware  {
    private final Logger logger = LoggerFactory.getLogger(PolicyConsumer.class);
    @Autowired
    private KafkaTemplate<Object, Object> template;

    @KafkaListener(topics = "policy-topic")
    public void processInvoice(String incomingPolicy,
                               @Header(KafkaHeaders.OFFSET) Long offset,
                               Acknowledgment ack) throws InterruptedException {
        logger.info("Processing Policy: " + incomingPolicy);
        logger.info("With offset: " + offset);
        Thread.sleep(3000);
        logger.info("Done Processing Policy: " + incomingPolicy);
    }
}
