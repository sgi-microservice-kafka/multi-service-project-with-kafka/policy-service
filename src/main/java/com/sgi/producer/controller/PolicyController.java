package com.sgi.producer.controller;

import com.sgi.producer.PolicyService;
import com.sgi.producer.common.Policy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class PolicyController {
    private final Logger logger = LoggerFactory.getLogger(PolicyService.class);

    public static ArrayList<Policy> policyList = new ArrayList<Policy>();
    @Autowired
    private KafkaTemplate<Object, Object> template;

    public PolicyController() {
        this.policyList.add(new Policy("POL001", "POL001", "Jiwa", "HOL001",
                "Badu"));
        this.policyList.add(new Policy("POL002", "POL002", "Kesehatan", "HOL001",
                "Badu"));
        this.policyList.add(new Policy("POL003", "POL003", "Jiwa", "HOL002",
                "Budi"));
    }

    @GetMapping(path = "/api/policy")
    public ArrayList<Policy> getAllInvoice() {
        logger.info("Gell All Policy");
        return this.policyList;
    }

    @GetMapping(path = "/api/policy/{policyId}")
    public Policy getPolicyById(@PathVariable String policyId) {
        logger.info("Get Policy of "+policyId);
        for(Policy policy: policyList){
            if(policyId.equals(policy.getPolicyId())){
                return policy;
            }
        }
        return null;
    }
    @PostMapping(path = "/api/policy/add")
    public void addNewInvoice(@RequestBody Policy newpolicy) {
        logger.info("Add new policy : " + newpolicy.toString());
        this.policyList.add(newpolicy);
    }
}
