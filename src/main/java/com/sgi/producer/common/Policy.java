package com.sgi.producer.common;


/**
 * @author Gary Russell
 * @since 2.2.1
 *
 */
public class Policy {

    private String policyId;
    private String policyNo;
    private String productName;
    private String holderId;
    private String holderName;
    private boolean policyStatus;

    public Policy() {
        this.policyStatus = true;
    }

    public Policy(String policyId, String policyNo, String productName, String holderId, String holderName) {
        this.policyId = policyId;
        this.policyNo = policyNo;
        this.productName = productName;
        this.holderId = holderId;
        this.holderName = holderName;
        this.policyStatus = true;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getHolderId() {
        return holderId;
    }

    public void setHolderId(String holderId) {
        this.holderId = holderId;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public boolean isPolicyStatus() {
        return policyStatus;
    }

    public void setPolicyStatus(boolean policyStatus) {
        this.policyStatus = policyStatus;
    }

    public String toString() {
        return ""+this.policyId+" "+this.policyStatus;
    }
}