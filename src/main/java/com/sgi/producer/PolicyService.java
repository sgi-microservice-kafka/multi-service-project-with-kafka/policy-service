package com.sgi.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolicyService {
	private final Logger logger = LoggerFactory.getLogger(PolicyService.class);
	public static void main(String[] args) {
		SpringApplication.run(PolicyService.class, args);
	}
}
